function [T,E]=padZero(T,E)
    for i = 1:1:size(T,1)
        for j = 1:1:size(T,2)
            if T(i,j) == 0
                T(i,j) = 1e-10;
            end
        end
    end
    for i = 1:1:size(E,1)
        for j = 1:1:size(E,2)
            if E(i,j) == 0
                E(i,j) = 1e-10;
            end
        end
    end
end