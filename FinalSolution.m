observations = csvread('observations.csv');
labels = csvread('labels.csv');


%Test using all possible location labels AND the first 100 labelled locations as training set, the given next 100 labelled steps as test set.
% test_using_loop_values = true;

%Use the first 100 labelled locations as training set and next 100 labelled locations test set.
test_using_loop_values = false;

labelled_data_set = generateLabelledDataSet(observations,labels,test_using_loop_values);
for num_of_hidden_states = 48:1:48
    
    % For each bot, generate HMM model #1: 
    %       Observation -> Distance
    %       Hidden States -> Some Unknown Latent Variables
    [T_map, E_map] = generateTAndEForEachBot(observations,num_of_hidden_states);
    
    % Use HMM model #1 to predict the most likely state in each step in
    % each run
    [states,bot_runs] = findMostLikelyBotForEachRunAndStateNumberForEachStep(T_map, E_map, labelled_data_set);
    
    % For each bot, get the sequence of states with known locations.
    % Also get the sequence of the corresponding locations
    [training_data_state_seq, training_data_location_seq] = getTrainingData(labelled_data_set, states, bot_runs, test_using_loop_values);
    
    % For each bot, generate HMM model #2: 
    %       Observation -> Location
    %       Hidden States -> The exact same states from HMM model #1
    E_estimate_map = containers.Map('KeyType','double','ValueType','any');
    for bot_index = 1:1:3
        [~,E_estimate] = hmmestimate(training_data_location_seq(bot_index),training_data_state_seq(bot_index));
        E_estimate_map(bot_index) = E_estimate;
    end
    
    % For each run, identify which bot it is
    % Then use its HMM model #2 to find out which location has the highest
    % P(Location = l|State = mostLikelyState)
    %
    % Note that the mostLikelyState is already determined in the 
    % findMostLikelyBotForEachRunAndStateNumberForEachStep method above
    [accuracy_for_101_to_200_runs,accuracy_for_201_to_3000_runs] = predict(num_of_hidden_states,E_estimate_map, states, bot_runs,labelled_data_set);
    fprintf('For %d hidden states, the accuracy is %d and %d\n', num_of_hidden_states, accuracy_for_101_to_200_runs, accuracy_for_201_to_3000_runs);
end