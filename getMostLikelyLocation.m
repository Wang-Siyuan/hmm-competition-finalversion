function location_value=getMostLikelyLocation(run_index, E_estimate_map, states, bot_runs)
    bot_index = bot_runs(run_index);
    E_estimate = E_estimate_map(bot_index);
    state = states(run_index,100);
    if state > size(E_estimate,1)
        location_value = 0;%invalid value, indicating our model failed to make a valid prediction
    else
        [max_val, max_index] = max(E_estimate(state,:));
        location_value = max_index;
    end
end