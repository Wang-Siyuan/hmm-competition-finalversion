function labelled_data_set = generateLabelledDataSet(observations,labels,test_using_loop_values)
    labelled_data_set = zeros(3000,200);
    for i = 1:1:size(labelled_data_set,1)
        labelled_data_set(i,1:100) = encodeX(observations(i,:));
        for j = 1:1:100
            if getLocation(observations(i,j)) > 0
                labelled_data_set(i,100+j) = getLocation(observations(i,j));
            end
        end
        if i <= 200
            if labelled_data_set(i,200) > 0 && labelled_data_set(i,200) ~= labels(i)
                ERROR = 1
            end
            labelled_data_set(i,200) = labels(i);
        end
    end
    % min consecutive path length 5
    if test_using_loop_values == true
        labelled_data_set = labelLoop(observations,labelled_data_set,5);
    end
%     fprintf('Initialized all labeled data\n');
end


function labelled_data_set = labelLoop(observations,labelled_data_set,len) 
   eu = EU(); 
   dists = [eu(2,2),eu(3,2),eu(4,2),eu(4,3),eu(4,4),eu(3,4),eu(2,4),eu(2,3)];
   labels = [7,12,17,18,19,14,9,9]; 
   dists = [dists dists];
   labels = [labels labels]; 
   l = len - 1; 
   for i = 1:3000
       for j = 1:(100 - l) 
           id = find(dists == observations(i,j)); 
           if id > 0 & sum(dists(id:id+l) == observations(i,j:(j+l))) == len
               labelled_data_set(i,(100+j):(100+j+l)) = labels(id:(id + l)); 
           end 
       end
   end 
end

function eu = EU() 
   eu = zeros(5,5);
   for i = 0:1:4
      for j = 1:1:5
          eu(i+1,j) = round(sqrt(i^2 + j^2),5,'significant');
      end
   end
end