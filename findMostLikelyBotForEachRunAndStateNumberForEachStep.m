function [states,bot_runs] = findMostLikelyBotForEachRunAndStateNumberForEachStep(T_map, E_map, labelled_data_set)
    states = zeros(3000,100);
    bot_runs = zeros(3000,1);
    for i = 1:1:3000
%         if mod(i,500) == 0
%             fprintf('Finished getting state values for %d/3000 runs\n', i);
%         end
        probabilities = zeros(3,1);
        PSTATES_map = containers.Map('KeyType','double','ValueType','any');
        for bot_index = 1:1:3
            [PSTATES,logpseq] = hmmdecode(labelled_data_set(i,1:100),T_map(bot_index),E_map(bot_index));
            PSTATES_map(bot_index) = PSTATES;
            probabilities(bot_index) = logpseq;
        end
        [~, max_index] = max(probabilities);
        bot_in_use = max_index;
        bot_runs(i) = bot_in_use;
        PSTATES_in_use = PSTATES_map(bot_in_use);
        for j = 1:1:100
            [max_val, max_index] = max(PSTATES_in_use(:,j));
            states(i,j) = max_index;
        end
    end
end