function [T_map, E_map] = generateTAndEForEachBot(observations,num_of_hidden_states)
    maxiter = 5000;
    T_map = containers.Map('KeyType','double','ValueType','any');
    E_map = containers.Map('KeyType','double','ValueType','any');
    seq_1 = {encodeX(observations(1,:)),encodeX(observations(4,:)),encodeX(observations(10,:)),encodeX(observations(13,:)), encodeX(observations(5,:)),encodeX(observations(18,:))};
    seq_2 = {encodeX(observations(2839,:)),encodeX(observations(2854,:)),encodeX(observations(2908,:)),encodeX(observations(2915,:)), encodeX(observations(2795,:)),encodeX(observations(2794,:))};
    seq_3 = {encodeX(observations(2969,:)),encodeX(observations(2967,:)),encodeX(observations(2962,:)),encodeX(observations(2935,:)), encodeX(observations(2931,:)),encodeX(observations(2895,:))};
    bot_index_set = [1,2,3];
    seq_set = {seq_1,seq_2,seq_3};
    seq_map = containers.Map(bot_index_set, seq_set);
    for i = 1:1:3
        T_initial = rand(num_of_hidden_states,num_of_hidden_states);
        E_initial = rand(num_of_hidden_states,14);
        [T_trained,E_trained] = hmmtrain(seq_map(i),T_initial,E_initial,'Maxiterations', maxiter);
        [T_trained_zero_padded, E_trained_zero_padded] = padZero(T_trained,E_trained);
        T_map(i) = T_trained_zero_padded;
        E_map(i) = E_trained_zero_padded;
    end
%     fprintf('Finished training T and E\n');
end