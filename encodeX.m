%% Encoding observation value X into 1,2,3,4,5. TODO: Needs confirmation from TA about our interpretation
function X_encoded = encodeX(X)
    X_encoded = zeros(size(X));
    for i = 1:size(X,2)
       if approxEquals(X(i), 1)
           X_encoded(1,i) = 1;
       elseif approxEquals(X(i), 1.4142)
           X_encoded(1,i) = 2;
       elseif approxEquals(X(i), 2)
           X_encoded(1,i) = 3;
       elseif approxEquals(X(i), 2.2361)
           X_encoded(1,i) = 4;
       elseif approxEquals(X(i),2.8284)
           X_encoded(1,i) = 5;
       elseif approxEquals(X(i),3)
           X_encoded(1,i) = 6;
       elseif approxEquals(X(i), 3.1623)
           X_encoded(1,i) = 7;
       elseif approxEquals(X(i), 3.6056)
           X_encoded(1,i) = 8;
       elseif approxEquals(X(i), 4)
           X_encoded(1,i) = 9;
       elseif approxEquals(X(i), 4.1231)
           X_encoded(1,i) = 10;
       elseif approxEquals(X(i), 4.2426)
           X_encoded(1,i) = 11;
       elseif approxEquals(X(i), 4.4721)
           X_encoded(1,i) = 12;
       elseif approxEquals(X(i), 5)
           X_encoded(1,i) = 13;
       elseif approxEquals(X(i), 5.6569)
           X_encoded(1,i) = 14;
       elseif approxEquals(X(i), 5.0990)
           X_encoded(1,i) = 15;
       elseif approxEquals(X(i),5.3852)
           X_encoded(1,i) = 16;
       elseif approxEquals(X(i),5.8310)
           X_encoded(1,i) = 17;
       elseif approxEquals(X(i),6.4031)
           X_encoded(1,i) = 18;
       else
           X_encode_error = 1
       end
    end
end

function result = approxEquals(a,b)
    result = abs(a-b) < 0.01;
end