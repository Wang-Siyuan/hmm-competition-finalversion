function location = getLocation(x)
    if x == 1
        location = 1;
    elseif x == 1.4142;
        location = 6;
    elseif x == 2;
        location = 2;
    elseif x == 3;
        location = 3;
    elseif x == 2.8284;
        location = 12;
    elseif x == 4;
        location = 4;
    elseif x == 4.2426
        location = 18;
    elseif x == 5.6569
        location = 24;
    else
        location = -1;
    end
end