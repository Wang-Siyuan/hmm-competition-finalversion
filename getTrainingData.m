function [training_data_state_seq, training_data_location_seq] = getTrainingData(labelled_data_set, states, bot_runs, test_using_loop_values)
    training_data_state_seq = containers.Map('KeyType','double','ValueType','any');
    training_data_location_seq = containers.Map('KeyType','double','ValueType','any');
    for bot_index = 1:1:3
%         fprintf('Started gathering training data for bot %d\n', bot_index);
        individual_bot_states_seq = [];
        individual_bot_location_seq = [];
        if test_using_loop_values == true
            for i = 1:1:3000
                bot_index_of_run = bot_runs(i);
                if bot_index_of_run == bot_index
                    for j = 1:1:99
                        if labelled_data_set(i,j+100) > 0
                            individual_bot_states_seq = [individual_bot_states_seq states(i,j)];
                            individual_bot_location_seq = [individual_bot_location_seq labelled_data_set(i,j+100)];
                        end
                    end
                end
            end
        end
        % Always include the first 100 last steps(the given labels) in the training
        % data.
        for i = 1:1:100
            individual_bot_states_seq = [individual_bot_states_seq states(i,100)];
            individual_bot_location_seq = [individual_bot_location_seq labelled_data_set(i,200)];
        end
        training_data_state_seq(bot_index) = individual_bot_states_seq;
        training_data_location_seq(bot_index) = individual_bot_location_seq;
    end
end