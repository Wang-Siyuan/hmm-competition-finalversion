function [accuracy_for_101_to_200_runs,accuracy_for_201_to_3000_runs] = predict(num_of_hidden_states, E_estimate_map, states, bot_runs,labelled_data_set)
    successCount = 0;
    for i = 101:1:200
        location_value=getMostLikelyLocation(i, E_estimate_map, states, bot_runs);
        if labelled_data_set(i,200) == location_value
            successCount = successCount + 1;
        end
    end
    accuracy_for_101_to_200_runs = successCount/100;
%     fprintf('Prediction for 101 to 200 labelled data has accuracy %d\n', accuracy_for_101_to_200_runs);
    
    result = zeros(3000,2);
    successCount = 0;
    totalCount = 0;
    for i = 201:1:3000
        result(i,1) = i-200;
        result(i,2) = getMostLikelyLocation(i, E_estimate_map, states, bot_runs);
        if labelled_data_set(i,200) > 0 
            totalCount = totalCount + 1;
            if labelled_data_set(i,200) ~= result(i,2)
                result(i,2) = labelled_data_set(i,200);
%                 fprintf('Wrong prediction at %d. True value should be %d.\n',i,labelled_data_set(i,200));
            else
                successCount = successCount + 1;
            end
        end
    end
    submitResult = result(201:3000,:);
    csvwrite(strcat('predictions', num2str(num_of_hidden_states), '.csv'),submitResult);
    g = csvread('best.csv');
    fprintf('Percent similar to best so far is : %f \n', sum(submitResult(:,2) == g(:,2)) / 28); 
    accuracy_for_201_to_3000_runs  = successCount/totalCount;
%     fprintf('Prediction for all the known locations at the final step of 201 to 300 runs have accuracy %d\n', accuracy_for_201_to_3000_runs);
end